Vue.component('utilisateurs', {
    data: function () {
        return {
            input_user: '',
            users: [
                'ISNARD',
                'MILLAN'
            ]
        }
    },
    template : `<div>
                    <input type="text"   v-model="input_user">
                    <input type="button" value="Ajouter" @click="addUser(input_user)">
                    <p>{{input_user}}</p>
                    <ul v-if="users.length > 0">
                        <li v-for="(user, index) in users">
                            {{ user }} <input type="button" value="Supprimer" @click="deleteUser(index)">
                        </li>
                    </ul>
                    <p v-else> Liste est vide. </p>
                </div>`,
    methods: {
        addUser: function (p_user) {
            this.users.push(p_user)
        },
        deleteUser: function (p_index) {
            this.users.splice(p_index, 1)
        }
    }
  });

let app = new Vue({
                    el: '#app'
                });